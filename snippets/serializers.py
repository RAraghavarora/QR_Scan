from rest_framework import serializers
from snippets.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('bitsid', 'uid', 'qr_code', 'no_of_entries')

    def update(self,instance,validated_data):
        instance.no_of_entries = validated_data.get('no_of_entries',instance.no_of_entries) - 1
        instance.save()
        return instance
