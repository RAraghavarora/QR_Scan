from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    bitsid = models.CharField(max_length=15,null=True)
    uid = models.CharField(max_length=30,null=False)
    qr_code = models.CharField(max_length=30)
    no_of_entries = models.IntegerField()
    user=models.OneToOneField(User,on_delete=models.CASCADE)
