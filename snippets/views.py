#from django.shortcuts import render
from snippets.models import Profile
from snippets.serializers import ProfileSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# Create your views here.

class scan(APIView):

    def get_object(self, qr_code):
        try:
            return Profile.objects.get(qr_code=qr_code)
        except Profile.DoesNotExist:
            raise Http404

    def get(self, request, qr_code, format=None):
        profile = self.get_object(qr_code)
        serializer = ProfileSerializer(profile)
        #print(type(request.data))
        return Response(serializer.data)

    def put(self, request, qr_code, format=None):
        profile = self.get_object(qr_code)
        serializer = ProfileSerializer(profile, data={'bitsid':profile.bitsid,'uid':profile.uid,
        'qr_code':qr_code,'no_of_entries':profile.no_of_entries})
        if serializer.is_valid():
            profile.no_of_entries = profile.no_of_entries - 1

            profile.save()
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, qr_code, format=None):
        profile = self.get_object(qr_code)
        profile.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
