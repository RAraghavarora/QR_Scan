from rest_framework_jwt.views import obtain_jwt_token
from django.conf.urls import url,include
from . import views
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token

app_name='snippets'

urlpatterns = [
    #url(r'^hello/$', views.a),

    url(r'^api-token-auth/$', obtain_jwt_token),
    url(r'^verify-api-token',verify_jwt_token),
    url(r'^scan/(?P<qr_code>[0-9 a-z A-Z]+)/$$',views.scan.as_view()),
    #url(r'^display_detail/$',views.display_detail.as_view())
]
